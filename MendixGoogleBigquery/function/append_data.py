
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
import requests
import datetime
import pandas as pd
desired_width = 320
pd.set_option('display.width', desired_width)


from googleapiclient import discovery
from oauth2client.client import GoogleCredentials
from google.cloud import bigquery
import google.auth

project = "incentroba-216709"
bucket_name = "mendixpraegus"
dataset = "mendixpraegustestdata"



def extract_data_store_to_bigquery_append(table, event = None, context = None):
    destination_table = dataset + "." + table
    print("Extracting data from Mendix application")
    #
    # Use changedDate attribute to only retrieve objects changed since yesterday

    yesterday = datetime.date.today() - datetime.timedelta(days=1)
    yesterdayStr = yesterday.strftime("%Y-%m-%dT00:00:00+01:00")
    filterParameter = "?$filter=createdDate gt datetimeoffset'{}'".format(yesterdayStr)

    # Define the endpoint of the odata resource
    #
    odataPath = "{}{}{}".format("/odata/Praegus/", table,"()")
    print(odataPath)
    odataUrl = "{}{}{}".format("https://incentropartnerkit-accp.mendixcloud.com",odataPath,filterParameter)
    print("Retrieving odata resource: {}".format(odataUrl))
    #
    # get the data from the Mendix odata endpoint
    #
    odataUsr = "Odata"
    odataPwd = "Welkom0!"
    req = requests.get(odataUrl,auth=(odataUsr,odataPwd))
    #
    # Define odata namespaces used by the xml returned
    #
    nsa = "{http://www.w3.org/2005/Atom}"
    nsm = "{http://schemas.microsoft.com/ado/2007/08/dataservices/metadata}"
    nsd = "{http://schemas.microsoft.com/ado/2007/08/dataservices}"

    # Use ElementTree to parse Odata feed
    row = 0
    root = ET.fromstring(req.content)
    entries = root.findall(".//{0}entry".format(nsa))

    for entry in entries:
        property = entry.findall("./{0}content/{1}properties/*".format(nsa, nsm))

            # Write out a first line with header names

        if row == 0:
                nameList = [name.tag.split("}")[1] for name in property]

                # To avoid error message in Excel, we need to change ID to Id
                # Excel has detected that ... is a SYLK file, but cannot load it. Either the file has errors or it is not
                # a SYLK file format. Click OK to try to open the file in a different format.
                if (nameList[0] == u"ID"):
                    nameList[0] = u"Id"
                row = row + 1

                # Create dictionary with keys from header row. Dictionary will be used to make Panda Dataframe
                dict = {el:[] for el in nameList}

        # Create list for each row
        valueList = [p.text.encode('utf-8').decode("utf-8") if p.text is not None else "" for p in property]

        # add each values to appropriate key in dictionary
        a = 0
        for v in valueList:
            b = nameList[a]
            dict[b].append(v)
            a = a + 1

    # Create dataframe
    df = pd.DataFrame(dict)


    # Write the dataframe to bigquery
    # df.to_gbq(destination_table, project, if_exists='append')

def extract_data_store_to_bigquery_truncate(table, event = None, context = None):
    destination_table = dataset + "." + table
    print("Extracting data from Mendix application")

    # Define the endpoint of the odata resource

    odataPath = "{}{}{}".format("/odata/Praegus/", table,"()")
    print(odataPath)
    odataUrl = "{}{}".format("https://incentropartnerkit-accp.mendixcloud.com",odataPath)
    print("Retrieving odata resource: {}".format(odataUrl))

    # get the data from the mendix odata endpoint

    odataUsr = "Odata"
    odataPwd = "Welkom0!"
    req = requests.get(odataUrl,auth=(odataUsr,odataPwd))

    # Define odata namespaces used by the xml returned

    nsa = "{http://www.w3.org/2005/Atom}"
    nsm = "{http://schemas.microsoft.com/ado/2007/08/dataservices/metadata}"
    nsd = "{http://schemas.microsoft.com/ado/2007/08/dataservices}"

    # Use ElementTree to parse Odata feed
    row = 0
    root = ET.fromstring(req.content)
    entries = root.findall(".//{0}entry".format(nsa))

    for entry in entries:
        property = entry.findall("./{0}content/{1}properties/*".format(nsa, nsm))

            # Write out a first line with header names

        if row == 0:
                nameList = [name.tag.split("}")[1] for name in property]

                # To avoid error message in Excel, we need to change ID to Id
                # Excel has detected that ... is a SYLK file, but cannot load it. Either the file has errors or it is not
                # a SYLK file format. Click OK to try to open the file in a different format.
                if (nameList[0] == u"ID"):
                    nameList[0] = u"Id"
                row = row + 1

                # Create dictionary with keys from header row. Dictionary will be used to make Panda Dataframe
                dict = {el:[] for el in nameList}

        # Create list for each row
        valueList = [p.text.encode('utf-8').decode("utf-8") if p.text is not None else "" for p in property]

        # add each values to appropriate key in dictionary
        a = 0
        for v in valueList:
            b = nameList[a]
            dict[b].append(v)
            a = a + 1

    # Create dataframe
    df = pd.DataFrame(dict)

    if table == "PraegusTest":
        # Replace values
        df = df.replace("pass", "passed")
        # print(df.result)
        # split column in dataframe as data pre-processing
        df['Organization'] = df.testCaseId.str.split(".").str.get(0)
        df['TestClass']= df.testCaseId.str.split(".").str.get(1)
        df['TestName'] = df.testCaseId.str.split(".").str.get(2)
        # print(df.Organization.head())
        # delete unnecessary values
        df = df.dropna(subset=["TestName"])
        print(df.Organization.head())
        print(df.TestName.head())

    elif table == "PraegusMeasurement":
        # Replace values
        df = df.replace(-1, 0)
        print(df.milliseconds)
    else:
        print(df.head)
    # Write the dataframe to bigquery
    df.to_gbq(destination_table, project, chunksize=2000, if_exists='replace')


# Below the functions can be called when using local machine.
extract_data_store_to_bigquery_truncate("Organisation")
extract_data_store_to_bigquery_truncate("Stack")
extract_data_store_to_bigquery_truncate("PraegusMeasurement")
extract_data_store_to_bigquery_truncate("PraegusTest")
extract_data_store_to_bigquery_truncate("Result")





